export enum SupportedEmojis {
  MX = '🇲🇽',
  AR = '🇦🇷',
  CO = '🇨🇴',
  CL = '🇨🇱',
  ES = '🇪🇸',
  US = '🇺🇸',
  BR = '🇧🇷',
  ESPANHITA = '887017542256848936',
}

export enum SupportedLanguages {
  SPANISH = 'es',
  ENGLISH = 'en',
  PORTUGUESE = 'pt',
}

export const TRANSLATOR_LANGUAGES = {
  [SupportedLanguages.SPANISH]: [
    SupportedEmojis.MX,
    SupportedEmojis.AR,
    SupportedEmojis.CO,
    SupportedEmojis.CL,
    SupportedEmojis.ES,
    SupportedEmojis.ESPANHITA,
  ],
  [SupportedLanguages.ENGLISH]: [SupportedEmojis.US],
  [SupportedLanguages.PORTUGUESE]: [SupportedEmojis.BR],
};

export const TRANSLATED_TITLES = {
  [SupportedLanguages.SPANISH]: 'Traducción',
  [SupportedLanguages.ENGLISH]: 'Translation',
  [SupportedLanguages.PORTUGUESE]: 'Tradução',
};

export const TRANSLATED_SHORT_MESSAGE_DELETE_TIMEOUT = 10000;

export const TRANSLATED_MEDIUM_MESSAGE_DELETE_TIMEOUT = 20000;

export const TRANSLATED_LONG_MESSAGE_DELETE_TIMEOUT = 30000;

export const SHORT_MESSAGE_THRESHOLD = 300;

export const MEDIUM_MESSAGE_THRESHOLD = 1000;

export const TRANSLATOR_THUMBNAIL =
  'https://images-na.ssl-images-amazon.com/images/I/41Jq18y8bKL.png';
