import { ExtendsCommand } from '@decorators/extends-command';
import { SubcommandGroup } from '@decorators/subcommand-group';
import { AdminBaseGateway } from '@gateways/admin/admin-base';
import { Injectable } from '@nestjs/common';

@Injectable()
@ExtendsCommand()
@SubcommandGroup({
  name: 'autoroles',
  description: 'Configuración de mensajes que asignan roles a los usuarios',
})
export class AdminAutoRoleBaseGateway extends AdminBaseGateway {}
