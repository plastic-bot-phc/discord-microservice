import {
  CLIENT_ID_ENV,
  GUILD_TEST_ID_ENV,
  TOKEN_ENV,
} from '@constants/environment';
import {
  SlashCommandBuilder,
  SlashCommandSubcommandBuilder,
  SlashCommandSubcommandGroupBuilder,
} from '@discordjs/builders';
import { REST } from '@discordjs/rest';
import { CommandParameterType } from '@enums/command-parameter-type';
import { ICommandParameter } from '@interfaces/command-parameter';
import { IDiscordCommand } from '@interfaces/discord-command';
import { IDiscordSubcommand } from '@interfaces/discord-subcommand';
import { IDiscordSubcommandGroup } from '@interfaces/discord-subcommand-group';
import { IAddAdminRole } from '@interfaces/roles/add-admin-role';
import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { CommandDiscoveryService } from '@services/command-discovery';
import { DiscordRxService } from '@services/discord-rx';
import { Routes } from 'discord-api-types/v9';
import {
  ApplicationCommand,
  Client,
  Collection,
  CommandInteraction,
  Guild,
  GuildApplicationCommandPermissionData,
  Intents,
  MessageReaction,
  Role,
  User,
} from 'discord.js';
import { ApplicationCommandPermissionTypes } from 'discord.js/typings/enums';
import { Observable, Subscriber } from 'rxjs';

@Injectable()
export class DiscordClientService extends Client {
  private readonly logger: Logger = new Logger(DiscordClientService.name);

  constructor(
    private readonly configService: ConfigService,
    private readonly discovery: CommandDiscoveryService,
    private readonly discordRxService: DiscordRxService,
  ) {
    super({
      intents: [
        Intents.FLAGS.GUILDS,
        Intents.FLAGS.GUILD_MESSAGES,
        Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
      ],
      partials: ['MESSAGE', 'CHANNEL', 'REACTION'],
    });
  }

  connect(): Promise<string> {
    const token: string = this.configService.get<string>(TOKEN_ENV);
    return this.login(token);
  }

  onReady(): Observable<void> {
    return new Observable((subscriber: Subscriber<void>) => {
      this.on('ready', () => subscriber.next());
    });
  }

  onGuildCreate(): Observable<Guild> {
    return new Observable((subscriber: Subscriber<Guild>) => {
      this.on('guildCreate', (guild: Guild) => subscriber.next(guild));
    });
  }

  onInteractionCreate(): Observable<CommandInteraction> {
    return new Observable((subscriber: Subscriber<CommandInteraction>) => {
      this.on('interactionCreate', (interaction: CommandInteraction) =>
        subscriber.next(interaction),
      );
    });
  }

  onMessageReactionAdd(): Observable<Record<string, MessageReaction | User>> {
    return new Observable(
      (subscriber: Subscriber<Record<string, MessageReaction | User>>) => {
        this.on('messageReactionAdd', (reaction: MessageReaction, user: User) =>
          subscriber.next({ reaction, user }),
        );
      },
    );
  }

  onMessageReactionRemove(): Observable<
    Record<string, MessageReaction | User>
  > {
    return new Observable(
      (subscriber: Subscriber<Record<string, MessageReaction | User>>) => {
        this.on(
          'messageReactionRemove',
          (reaction: MessageReaction, user: User) =>
            subscriber.next({ reaction, user }),
        );
      },
    );
  }

  async updateCommands(): Promise<void> {
    const token: string = this.configService.get<string>(TOKEN_ENV);
    const clientId: string = this.configService.get<string>(CLIENT_ID_ENV);
    const rest = new REST({ version: '9' }).setToken(token);

    const slashCommands = this.buildCommands(this.discovery.getCommands());

    try {
      if (process.env.NODE_ENV === 'production') {
        await rest.put(Routes.applicationCommands(clientId), {
          body: slashCommands,
        });

        this.logger.log('Successfully updated application commands');
      } else {
        const guildId: string =
          this.configService.get<string>(GUILD_TEST_ID_ENV);

        await rest.put(Routes.applicationGuildCommands(clientId, guildId), {
          body: slashCommands,
        });

        this.logger.log('Successfully updated guild commands');
      }
    } catch (e) {
      this.logger.error(e.message);
      this.logger.error(e.stack);
    }
  }

  async updateCommandsOwnerAndAdminPermission(guildId: string): Promise<void> {
    try {
      const guild: Guild = await this.guilds.fetch(guildId);

      const highestRole: Role = guild.roles.highest;

      const commands: Collection<string, ApplicationCommand> =
        process.env.NODE_ENV === 'production'
          ? await this.application.commands.fetch()
          : await guild.commands.fetch();

      const protectedCommands: Collection<string, ApplicationCommand> =
        commands.filter((command) => !command.defaultPermission);

      const fullPermissions: GuildApplicationCommandPermissionData[] =
        protectedCommands.map((command) => ({
          id: command.id,
          permissions: [
            {
              id: guild.ownerId,
              permission: true,
              type: ApplicationCommandPermissionTypes.USER,
            },
            {
              id: highestRole.id,
              permission: true,
              type: ApplicationCommandPermissionTypes.ROLE,
            },
          ],
        }));

      // TODO: Remove this because Permissions v2
      // Fuck you Discord
      //await guild.commands.permissions.set({ fullPermissions });

      this.logger.log('Successfully updated permissions');
    } catch (e) {
      this.logger.error(e.message);
      this.logger.error(e.stack);
    }
  }

  async addCommandRolePermission(guildId: string, role: string): Promise<void> {
    try {
      const guild: Guild = await this.guilds.fetch(guildId);

      const commands: Collection<string, ApplicationCommand> =
        process.env.NODE_ENV === 'production'
          ? new Collection<string, ApplicationCommand>([
              ...(await this.application.commands.fetch()),
              ...(await guild.commands.fetch()),
            ])
          : await guild.commands.fetch();

      const protectedCommands: ApplicationCommand[] = commands
        .filter((command) => !command.defaultPermission)
        .toJSON();

      for (let command of protectedCommands) {
        const permissions = [
          {
            id: role,
            permission: true,
            type: ApplicationCommandPermissionTypes.ROLE,
          },
        ];

        await command.permissions.add({ permissions });
      }

      this.logger.log('Successfully added permissions');
    } catch (e) {
      this.logger.error(e.message);
      this.logger.error(e.stack);
    }
  }

  async removeCommandRolePermission(
    guildId: string,
    role: string,
  ): Promise<void> {
    try {
      const guild: Guild = await this.guilds.fetch(guildId);

      const commands: Collection<string, ApplicationCommand> =
        process.env.NODE_ENV === 'production'
          ? new Collection<string, ApplicationCommand>([
              ...(await this.application.commands.fetch()),
              ...(await guild.commands.fetch()),
            ])
          : await guild.commands.fetch();

      const protectedCommands: ApplicationCommand[] = commands
        .filter((command) => !command.defaultPermission)
        .toJSON();

      for (let command of protectedCommands) {
        await command.permissions.remove({ roles: [role] });
      }

      this.logger.log('Successfully removed permissions');
    } catch (e) {
      this.logger.error(e.message);
      this.logger.error(e.stack);
    }
  }

  handleAddAdminRole(): void {
    this.discordRxService
      .onAddRolePermission()
      .subscribe((data: IAddAdminRole) => {
        this.addCommandRolePermission(data.guild, data.role);
      });
  }

  private buildCommands(commands: IDiscordCommand[]): any[] {
    return commands.map((command) => {
      const builder = new SlashCommandBuilder();

      this.buildCommandOrSubcommand(builder, command);

      if (command.isAdminCommand) {
        builder.setDefaultPermission(false);
      }

      if (command.subcommandGroups.length > 0) {
        this.buildSubcommandGroups(builder, command.subcommandGroups);
      }

      if (command.subcommands.length > 0) {
        this.buildSubcommands(builder, command.subcommands);
      }

      return builder.toJSON();
    });
  }

  private buildCommandOrSubcommand(
    builder: SlashCommandBuilder | SlashCommandSubcommandBuilder,
    command: IDiscordCommand | IDiscordSubcommand,
  ): SlashCommandBuilder | SlashCommandSubcommandBuilder {
    builder.setName(command.name).setDescription(command.description);

    if (command.parameters) {
      command.parameters.forEach((parameter) =>
        this.buildParameter(builder, parameter),
      );
    }

    return builder;
  }

  private buildSubcommands(
    builder: SlashCommandBuilder | SlashCommandSubcommandGroupBuilder,
    subcommands: IDiscordSubcommand[],
  ): void {
    subcommands.forEach((subcommand) =>
      builder.addSubcommand(
        (option) =>
          this.buildCommandOrSubcommand(
            option,
            subcommand,
          ) as SlashCommandSubcommandBuilder,
      ),
    );
  }

  private buildSubcommandGroups(
    builder: SlashCommandBuilder,
    subcommandGroups: IDiscordSubcommandGroup[],
  ): void {
    subcommandGroups.forEach((subcommandGroup) =>
      builder.addSubcommandGroup((option) => {
        option
          .setName(subcommandGroup.name)
          .setDescription(subcommandGroup.description);

        this.buildSubcommands(option, subcommandGroup.subcommands);

        return option;
      }),
    );
  }

  private buildParameter(
    builder: SlashCommandBuilder | SlashCommandSubcommandBuilder,
    parameter: ICommandParameter,
  ): void {
    switch (parameter.type) {
      case CommandParameterType.String:
        builder.addStringOption((option) =>
          option
            .setName(parameter.name)
            .setDescription(parameter.description)
            .setRequired(!!parameter.required),
        );
        break;

      case CommandParameterType.Number:
        builder.addNumberOption((option) =>
          option
            .setName(parameter.name)
            .setDescription(parameter.description)
            .setRequired(!!parameter.required),
        );
        break;

      case CommandParameterType.Boolean:
        builder.addBooleanOption((option) =>
          option
            .setName(parameter.name)
            .setDescription(parameter.description)
            .setRequired(!!parameter.required),
        );
        break;

      case CommandParameterType.User:
        builder.addUserOption((option) =>
          option
            .setName(parameter.name)
            .setDescription(parameter.description)
            .setRequired(!!parameter.required),
        );
        break;

      case CommandParameterType.Channel:
        builder.addChannelOption((option) =>
          option
            .setName(parameter.name)
            .setDescription(parameter.description)
            .setRequired(!!parameter.required),
        );
        break;

      case CommandParameterType.Mentionable:
        builder.addMentionableOption((option) =>
          option
            .setName(parameter.name)
            .setDescription(parameter.description)
            .setRequired(!!parameter.required),
        );
        break;

      case CommandParameterType.Role:
        builder.addRoleOption((option) =>
          option
            .setName(parameter.name)
            .setDescription(parameter.description)
            .setRequired(!!parameter.required),
        );
        break;

      case CommandParameterType.Integer:
        builder.addIntegerOption((option) =>
          option
            .setName(parameter.name)
            .setDescription(parameter.description)
            .setRequired(!!parameter.required),
        );
        break;
    }
  }
}
