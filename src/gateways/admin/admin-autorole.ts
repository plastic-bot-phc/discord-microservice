import { ExtendsCommand } from '@decorators/extends-command';
import { ExtendsSubcommandGroup } from '@decorators/extends-subcommand-group';
import { Subcommand } from '@decorators/subcommand';
import { CommandParameterType } from '@enums/command-parameter-type';
import { AdminAutoRoleBaseGateway } from '@gateways/admin/admin-autorole-base';
import { AutoRoleMicroservice } from '@microservices/autorole';
import { Injectable, Logger } from '@nestjs/common';
import { AutoRoleCacheService } from '@services/autorole-cache';
import { DiscordUtilService } from '@services/discord-util';
import { APIRole } from 'discord-api-types';
import { CommandInteraction, Role } from 'discord.js';
import { RuntimeException } from 'exceptions/runtime';
import { from, Observable, of, switchMap, tap, throwError } from 'rxjs';

@Injectable()
@ExtendsCommand()
@ExtendsSubcommandGroup()
export class AdminAutoRoleGateway extends AdminAutoRoleBaseGateway {
  private readonly logger: Logger = new Logger(AdminAutoRoleGateway.name);

  constructor(
    private readonly autoroleMicroService: AutoRoleMicroservice,
    private readonly discordUtilService: DiscordUtilService,
    private readonly autoroleCacheService: AutoRoleCacheService,
  ) {
    super();
  }

  @Subcommand({
    name: 'agregar',
    description: 'Agrega una reacción de auto rol a un mensaje',
    parameters: [
      {
        name: 'id',
        description:
          'Identificador del mensaje para asignar roles a los usuarios',
        type: CommandParameterType.String,
        required: true,
      },
      {
        name: 'emoji',
        description:
          'Emoji al que deberá reaccionar el usuario para asignar el rol',
        type: CommandParameterType.String,
        required: true,
      },
      {
        name: 'rol',
        description: 'Rol que será asignado con la reacción del usuario',
        type: CommandParameterType.Role,
        required: true,
      },
    ],
  })
  addReaction(interaction: CommandInteraction): Observable<any> {
    const id: string = interaction.options.getString('id');
    const emoji: string = interaction.options.getString('emoji');
    const role: Role | APIRole = interaction.options.getRole('rol');

    return from(interaction.deferReply({ ephemeral: true })).pipe(
      switchMap(() =>
        this.discordUtilService.getMessageById(interaction.guild.channels, id),
      ),
      switchMap((message) =>
        message === null
          ? throwError(
              () =>
                new RuntimeException('El identificador del mensaje no existe'),
            )
          : of(message),
      ),
      tap(() => this.logger.log(`Message ${id} exists!`)),
      switchMap(() =>
        this.autoroleMicroService.addReaction(
          interaction.guildId,
          id,
          emoji,
          role.id,
        ),
      ),
      switchMap(() =>
        this.autoroleCacheService.set(interaction.guildId, id, emoji, role.id),
      ),
      switchMap(() =>
        interaction.editReply('Mensaje configurado exitosamente'),
      ),
    );
  }

  @Subcommand({
    name: 'eliminar',
    description: 'Elimina una reacción de auto rol a un mensaje',
    parameters: [
      {
        name: 'id',
        description:
          'Identificador del mensaje para asignar roles a los usuarios',
        type: CommandParameterType.String,
        required: true,
      },
      {
        name: 'emoji',
        description: 'Emoji que se usaba para la reacción',
        type: CommandParameterType.String,
        required: true,
      },
    ],
  })
  removeReaction(interaction: CommandInteraction): Observable<any> {
    const id: string = interaction.options.getString('id');
    const emoji: string = interaction.options.getString('emoji');

    return from(interaction.deferReply({ ephemeral: true })).pipe(
      switchMap(() =>
        this.discordUtilService.getMessageById(interaction.guild.channels, id),
      ),
      switchMap((message) =>
        message === null
          ? throwError(
              () =>
                new RuntimeException('El identificador del mensaje no existe'),
            )
          : of(message),
      ),
      tap(() => this.logger.log(`Message ${id} exists!`)),
      switchMap(() =>
        this.autoroleMicroService.removeReaction(
          interaction.guildId,
          id,
          emoji,
        ),
      ),
      switchMap(() =>
        this.autoroleCacheService.del(interaction.guildId, id, emoji),
      ),
      switchMap(() =>
        interaction.editReply(
          `El emoji ${emoji} dejará de asignar roles en el mensaje`,
        ),
      ),
    );
  }
}
