import { SUBCOMMAND_DECORATOR } from '@constants/decorator';
import { ISubcommandOptions } from '@interfaces/subcommand-options';

export const Subcommand =
  (options: ISubcommandOptions): MethodDecorator =>
  (
    target: Record<string, string>,
    propertyKey: string | symbol,
    descriptor: PropertyDescriptor,
  ) => {
    Reflect.defineMetadata(SUBCOMMAND_DECORATOR, options, target, propertyKey);
    return descriptor;
  };
