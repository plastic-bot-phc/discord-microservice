import { GUILD_TEST_ID_ENV } from '@constants/environment';
import { ICommandMetadata } from '@interfaces/command-metadata';
import { IReactionMetadata } from '@interfaces/reaction-metadata';
import { Injectable, Logger, OnApplicationBootstrap } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { CommandDiscoveryService } from '@services/command-discovery';
import { DiscordClientService } from '@services/discord-client';
import { ReactionDiscoveryService } from '@services/reaction-discovery';
import {
  Collection,
  CommandInteraction,
  MessageReaction,
  OAuth2Guild,
  User,
} from 'discord.js';
import { RuntimeException } from 'exceptions/runtime';
import {
  catchError,
  combineLatest,
  filter,
  from,
  Observable,
  of,
  switchMap,
  tap,
  mergeMap,
} from 'rxjs';
import { AutoRoleCacheService } from './autorole-cache';
import { DiscordUtilService } from './discord-util';

@Injectable()
export class BotService implements OnApplicationBootstrap {
  private readonly logger: Logger = new Logger(BotService.name);

  constructor(
    private readonly client: DiscordClientService,
    private readonly configService: ConfigService,
    private readonly commandDiscovery: CommandDiscoveryService,
    private readonly reactionDiscovery: ReactionDiscoveryService,
    private readonly autoroleCacheService: AutoRoleCacheService,
    private readonly discordUtilService: DiscordUtilService,
  ) {}

  async onApplicationBootstrap(): Promise<void> {
    this.commandDiscovery.discoverCommands();
    this.reactionDiscovery.discoverReactions();

    await this.connect();
    await this.updateCommandsAndPermissions();
  }

  async connect(): Promise<void> {
    this.onReady();
    this.onGuildCreate();
    this.onInteractionCreate();
    this.onMessageReactionAdd();
    this.onMessageReactionRemove();

    await this.client.connect();

    this.client.handleAddAdminRole();
  }

  async updateCommandsAndPermissions(): Promise<void> {
    await this.client.updateCommands();

    if (process.env.NODE_ENV !== 'production') {
      const guildId = this.configService.get<string>(GUILD_TEST_ID_ENV);
      await this.client.updateCommandsOwnerAndAdminPermission(guildId);
    }
  }

  onReady(): void {
    this.client
      .onReady()
      .pipe(
        tap(() => this.logger.log('[onReady] Connected to Discord')),
        switchMap(() => this.client.guilds.fetch()),
        switchMap((guilds: Collection<string, OAuth2Guild>) =>
          from(guilds.toJSON()),
        ),
        tap((guild: OAuth2Guild) =>
          this.logger.log(`[onReady] Guild ${guild.id}`),
        ),
        mergeMap((guild: OAuth2Guild) =>
          this.autoroleCacheService.sync(guild.id),
        ),
      )
      .subscribe();
  }

  onGuildCreate(): void {
    this.client.onGuildCreate().subscribe((guild) => {
      this.client.updateCommandsOwnerAndAdminPermission(guild.id);
    });
  }

  onInteractionCreate() {
    this.client
      .onInteractionCreate()
      .pipe(
        filter((interaction: CommandInteraction) => interaction.isCommand()),
        switchMap((interaction: CommandInteraction) =>
          combineLatest([
            of(interaction),
            of(this.findCommandMetadata(interaction)),
          ]),
        ),
        filter(
          ([_, command]: [CommandInteraction, ICommandMetadata]) => !!command,
        ),
        switchMap(
          ([interaction, command]: [CommandInteraction, ICommandMetadata]) => {
            const { instance, method } = command;

            const interactionExecution = instance[method](interaction);

            const observableInteraction: Observable<unknown> =
              interactionExecution instanceof Observable
                ? interactionExecution
                : from(interactionExecution);

            return observableInteraction.pipe(
              catchError((err) => {
                this.logger.error(err);

                if (err.stack) {
                  this.logger.error(err.stack);
                }

                const defaultErrorMessage =
                  'Oops... ocurrió un error inesperado al procesar el comando. Intenta de nuevo';

                const content = `**ERROR:**   ${
                  err?.status === 'error' || err instanceof RuntimeException
                    ? err.message
                    : defaultErrorMessage
                }`;

                return interaction.deferred
                  ? interaction.editReply({
                      content,
                    })
                  : interaction.reply({
                      content,
                      ephemeral: true,
                    });
              }),
            );
          },
        ),
      )
      .subscribe();
  }

  onMessageReactionAdd() {
    this.client
      .onMessageReactionAdd()
      .pipe(
        switchMap(
          ({ reaction, user }: Record<string, MessageReaction | User>) =>
            combineLatest([
              reaction.partial ? reaction.fetch() : of(reaction),
              of(user),
            ]),
        ),
        switchMap(([reaction, user]: [MessageReaction, User]) => {
          const messageReaction: MessageReaction = <MessageReaction>reaction;
          const guildId = messageReaction.message.guildId;
          const messageId = messageReaction.message.id;

          const emoji =
            reaction.emoji.id !== null
              ? `<:${reaction.emoji.identifier}>`
              : reaction.emoji.name;

          return from(
            this.autoroleCacheService.get(guildId, messageId, emoji),
          ).pipe(
            switchMap((data: string) => {
              this.logger.log(
                `[onMessageReactionAdd] data from cache: ${data}`,
              );

              return data !== null
                ? this.discordUtilService.addRoleToUser(
                    reaction.message.guild,
                    user,
                    data,
                  )
                : of([reaction, user]);
            }),
          );
        }),
        filter((anything: any) => Array.isArray(anything)),
        switchMap(([reaction, user]: [MessageReaction, User]) =>
          combineLatest([
            of(reaction),
            of(user),
            of(this.findReactionMetadata(reaction)),
          ]),
        ),
        switchMap(
          ([reaction, user, metadata]: [
            MessageReaction,
            User,
            IReactionMetadata[],
          ]) =>
            Promise.all(
              metadata.map(({ instance, method }) =>
                instance[method](reaction, user),
              ),
            ),
        ),
        catchError((err) => {
          this.logger.error(err);
          return of(null);
        }),
      )
      .subscribe();
  }

  onMessageReactionRemove() {
    this.client
      .onMessageReactionRemove()
      .pipe(
        switchMap(
          ({ reaction, user }: Record<string, MessageReaction | User>) =>
            combineLatest([
              reaction.partial ? reaction.fetch() : of(reaction),
              of(user),
            ]),
        ),
        switchMap(([reaction, user]: [MessageReaction, User]) => {
          const messageReaction: MessageReaction = <MessageReaction>reaction;
          const guildId = messageReaction.message.guildId;
          const messageId = messageReaction.message.id;

          const emoji =
            reaction.emoji.id !== null
              ? `<:${reaction.emoji.identifier}>`
              : reaction.emoji.name;

          return from(
            this.autoroleCacheService.get(guildId, messageId, emoji),
          ).pipe(
            switchMap((data: string) => {
              this.logger.log(
                `[onMessageReactionRemove] data from cache: ${data}`,
              );

              return data !== null
                ? this.discordUtilService.removeRoleToUser(
                    reaction.message.guild,
                    user,
                    data,
                  )
                : of([reaction, user]);
            }),
          );
        }),
        catchError((err) => {
          this.logger.error(err);
          return of(null);
        }),
      )
      .subscribe();
  }

  private findReactionMetadata(reaction: MessageReaction): IReactionMetadata[] {
    return this.reactionDiscovery
      .getMetadata()
      .filter(({ metadata }) =>
        metadata.emojis.includes(reaction.emoji.id || reaction.emoji.name),
      );
  }

  private findCommandMetadata(
    interaction: CommandInteraction,
  ): ICommandMetadata {
    return this.commandDiscovery
      .getMetadata()
      .filter(({ command }) => command.name === interaction.commandName)
      .find((command) => {
        if (!command.subcommand && !command.subcommandGroup) {
          return command;
        }

        if (command.subcommandGroup && command.subcommand) {
          const subcommandGroupName = interaction.options.getSubcommandGroup();

          const subcommandName = interaction.options.getSubcommand();

          if (
            subcommandGroupName &&
            command.subcommandGroup.name === subcommandGroupName &&
            subcommandName === command.subcommand.name
          ) {
            return command;
          }
        }

        if (!command.subcommandGroup && command.subcommand) {
          const subcommandName = interaction.options.getSubcommand();

          if (subcommandName && command.subcommand.name === subcommandName) {
            return command;
          }
        }

        return false;
      });
  }
}
