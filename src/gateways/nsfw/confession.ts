import { Command } from '@decorators/command';
import { CommandParameterType } from '@enums/command-parameter-type';
import { AdminMicroservice } from '@microservices/admin';
import { Injectable, Logger } from '@nestjs/common';
import { CommandInteraction, MessageEmbed, TextChannel } from 'discord.js';
import { sample } from 'lodash';
import { filter, from, Observable, switchMap, tap } from 'rxjs';

@Injectable()
export class ConfessionGateway {
  private readonly logger: Logger = new Logger(ConfessionGateway.name);

  constructor(private readonly adminMicroservice: AdminMicroservice) {}

  @Command({
    name: 'confesion',
    description: 'Cuéntanos tus más oscuros secretos de manera anónima',
    parameters: [
      {
        name: 'mensaje',
        description: 'No le diremos nada a nadie',
        type: CommandParameterType.String,
        required: true,
      },
      {
        name: 'quitar-anonimato',
        description:
          'Si es activada ésta opción (True), la confesión será pública',
        type: CommandParameterType.Boolean,
      },
    ],
  })
  sendConfession(interaction: CommandInteraction): Observable<any> {
    const { guild, guildId, options } = interaction;
    const { channels } = guild;
    const message = options.getString('mensaje');
    const isPublic = options.getBoolean('quitar-anonimato');

    const emojis: string[] = [
      '😆',
      '🤨',
      '🤔',
      '😯',
      '🤐',
      '😲',
      '😦',
      '😏',
      '🤫',
    ];

    const embededMessage = new MessageEmbed().setDescription(message);

    if (isPublic) {
      embededMessage
        .setTitle(`Confesión de ${interaction.user.username} ${sample(emojis)}`)
        .setColor('#69F0AE')
        .setThumbnail(interaction.user.avatarURL());
    } else {
      embededMessage
        .setTitle(`Confesión anónima ${sample(emojis)}`)
        .setColor('#EE2625');
    }

    return from(interaction.deferReply({ ephemeral: true })).pipe(
      switchMap(() => this.adminMicroservice.getConfessionChannel(guildId)),
      tap(
        (channel: string) =>
          !channel &&
          interaction.editReply(
            [
              'El canal para publicar confesiones no está configurado.',
              'Avisa a un Administrador o Moderador para que puedan configurarlo.',
            ].join('\n'),
          ),
      ),
      filter((channel: string) => !!channel),
      switchMap((channel: string) => channels.fetch(channel)),
      switchMap((channel: TextChannel) =>
        channel.send({ embeds: [embededMessage] }),
      ),
      switchMap(() => interaction.editReply('Confesión enviada')),
    );
  }
}
