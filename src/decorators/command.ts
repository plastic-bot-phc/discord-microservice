import { COMMAND_DECORATOR } from '@constants/decorator';
import { ICommandOptions } from '@interfaces/command-options';
import { isMethod } from '@utils/is-method';

export const Command =
  (options: ICommandOptions): MethodDecorator & ClassDecorator =>
  (
    target: any,
    propertyKey?: string | symbol,
    descriptor?: TypedPropertyDescriptor<any>,
  ) => {
    if (descriptor) {
      Reflect.defineMetadata(COMMAND_DECORATOR, options, target, propertyKey);
    } else {
      Reflect.defineMetadata(COMMAND_DECORATOR, options, target);

      for (const propertyName of Object.getOwnPropertyNames(target.prototype)) {
        if (!isMethod(target.prototype, propertyName)) {
          continue;
        }

        Reflect.defineMetadata(
          COMMAND_DECORATOR,
          options,
          target,
          propertyName,
        );
      }
    }

    return target;
  };
