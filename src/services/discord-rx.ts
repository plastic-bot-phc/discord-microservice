import { IAddAdminRole } from '@interfaces/roles/add-admin-role';
import { IRemoveAdminRole } from '@interfaces/roles/remove-admin-role';
import { Injectable } from '@nestjs/common';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class DiscordRxService {
  private readonly $addRolePermission = new Subject<IAddAdminRole>();
  private readonly $removeRolePermission = new Subject<IRemoveAdminRole>();

  emitAddRolePermission(guild: string, role: string): void {
    this.$addRolePermission.next({ guild, role });
  }

  onAddRolePermission(): Observable<IAddAdminRole> {
    return this.$addRolePermission.asObservable();
  }

  emitRemoveRolePermission(guild: string, role: string): void {
    this.$removeRolePermission.next({ guild, role });
  }

  onRemoveRolePermission(): Observable<IRemoveAdminRole> {
    return this.$removeRolePermission.asObservable();
  }
}
