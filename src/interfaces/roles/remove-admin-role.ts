export interface IRemoveAdminRole {
  guild: string;
  role: string;
}
