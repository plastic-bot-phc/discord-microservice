import { IDiscordSubcommand } from '@interfaces/discord-subcommand';
import { ISubcommandGroupOptions } from '@interfaces/subcommand-group-options';

export interface IDiscordSubcommandGroup extends ISubcommandGroupOptions {
  subcommands: IDiscordSubcommand[];
}
