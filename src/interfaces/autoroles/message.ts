import { IRole } from '@interfaces/autoroles/role';

export interface IMessage {
  _id: string;
  guild: string;
  message: string;
  customContent: string;
  roles: IRole[];
}
