import { REACTION_DECORATOR } from '@constants/decorator';
import { IReactionOptions } from '@interfaces/reaction-options';

export const Reaction =
  (options: IReactionOptions): MethodDecorator =>
  (
    target: Record<string, string>,
    propertyKey: string | symbol,
    descriptor: PropertyDescriptor,
  ) => {
    Reflect.defineMetadata(REACTION_DECORATOR, options, target, propertyKey);
    return descriptor;
  };
