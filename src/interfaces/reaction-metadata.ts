import { IReactionOptions } from '@interfaces/reaction-options';

export interface IReactionMetadata {
  instance: any;
  method: string;
  metadata: IReactionOptions;
}
