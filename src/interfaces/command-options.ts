import { ICommandBaseOptions } from '@interfaces/command-base-options';
import { ICommandParameter } from '@interfaces/command-parameter';

export interface ICommandOptions extends ICommandBaseOptions {
  parameters?: ICommandParameter[];
  isAdminCommand?: boolean;
}
