export interface ICommandBaseOptions {
  name: string;
  description: string;
}
