import { REACTION_DECORATOR } from '@constants/decorator';
import { IReactionMetadata } from '@interfaces/reaction-metadata';
import { IReactionOptions } from '@interfaces/reaction-options';
import { Injectable, Logger } from '@nestjs/common';
import { DiscoveryService, MetadataScanner } from '@nestjs/core';
import { InstanceWrapper } from '@nestjs/core/injector/instance-wrapper';
import { isObject } from 'lodash';

@Injectable()
export class ReactionDiscoveryService {
  private readonly logger: Logger = new Logger(ReactionDiscoveryService.name);
  private metadata: IReactionMetadata[];

  constructor(
    private readonly discoveryService: DiscoveryService,
    private readonly metadataScanner: MetadataScanner,
  ) {}

  discoverReactions(): void {
    this.metadata = this.scanReactions();
  }

  getMetadata(): IReactionMetadata[] {
    return this.metadata;
  }

  private scanReactions(): IReactionMetadata[] {
    this.logger.log('Scanning reactions');

    return this.discoveryService
      .getProviders()
      .map((wrapper: InstanceWrapper) => wrapper.instance)
      .filter((instance) => !!instance && isObject(instance))
      .map((instance) =>
        this.metadataScanner.scanFromPrototype<any, IReactionMetadata>(
          instance,
          Object.getPrototypeOf(instance),
          (methodName) => this.scanMetadata(instance, methodName),
        ),
      )
      .flat()
      .filter((instance) => !!instance);
  }

  private scanMetadata(
    instance: any,
    method: string,
  ): IReactionMetadata | null {
    const metadata: IReactionOptions = Reflect.getMetadata(
      REACTION_DECORATOR,
      instance,
      method,
    );

    if (!metadata) {
      return null;
    }

    return {
      instance,
      method,
      metadata,
    };
  }
}
