import { ICommandOptions } from '@interfaces/command-options';
import { ISubcommandGroupOptions } from '@interfaces/subcommand-group-options';
import { ISubcommandOptions } from '@interfaces/subcommand-options';

export interface ICommandMetadata {
  instance: any;
  method: string;
  command: ICommandOptions;
  subcommand?: ISubcommandOptions;
  subcommandGroup?: ISubcommandGroupOptions;
}
