import {
  COMMAND_DECORATOR,
  SUBCOMMAND_DECORATOR,
  SUBCOMMAND_GROUP_DECORATOR,
} from '@constants/decorator';
import { ICommandMetadata } from '@interfaces/command-metadata';
import { ICommandOptions } from '@interfaces/command-options';
import { IDiscordCommand } from '@interfaces/discord-command';
import { ISubcommandGroupOptions } from '@interfaces/subcommand-group-options';
import { ISubcommandOptions } from '@interfaces/subcommand-options';
import { Injectable, Logger } from '@nestjs/common';
import { DiscoveryService, MetadataScanner } from '@nestjs/core';
import { InstanceWrapper } from '@nestjs/core/injector/instance-wrapper';
import { isObject } from 'lodash';

@Injectable()
export class CommandDiscoveryService {
  private readonly logger: Logger = new Logger(CommandDiscoveryService.name);
  private metadata: ICommandMetadata[];
  private commands: IDiscordCommand[];

  constructor(
    private readonly discoveryService: DiscoveryService,
    private readonly metadataScanner: MetadataScanner,
  ) {}

  discoverCommands(): void {
    this.metadata = this.scanCommands();
    this.commands = this.getHierarchy(this.metadata);
  }

  getMetadata(): ICommandMetadata[] {
    return this.metadata;
  }

  getCommands(): IDiscordCommand[] {
    return this.commands;
  }

  private scanCommands(): ICommandMetadata[] {
    this.logger.log('Scanning commands');

    return this.discoveryService
      .getProviders()
      .map((wrapper: InstanceWrapper) => wrapper.instance)
      .filter((instance) => !!instance && isObject(instance))
      .map((instance) =>
        this.metadataScanner.scanFromPrototype<any, ICommandMetadata>(
          instance,
          Object.getPrototypeOf(instance),
          (methodName) => this.scanCommandsMetadata(instance, methodName),
        ),
      )
      .flat()
      .filter((instance) => !!instance);
  }

  private scanCommandsMetadata(
    instance: any,
    method: string,
  ): ICommandMetadata | null {
    const commandMetadata: ICommandOptions = Reflect.getMetadata(
      COMMAND_DECORATOR,
      instance,
      method,
    );

    if (!commandMetadata) {
      return null;
    }

    const subcommandMetadata: ISubcommandOptions = Reflect.getMetadata(
      SUBCOMMAND_DECORATOR,
      instance,
      method,
    );

    const subcommandGroupMetadata: ISubcommandGroupOptions =
      Reflect.getMetadata(SUBCOMMAND_GROUP_DECORATOR, instance, method);

    return {
      instance,
      method,
      command: commandMetadata,
      subcommand: subcommandMetadata,
      subcommandGroup: subcommandGroupMetadata,
    };
  }

  private getHierarchy(commands: ICommandMetadata[]): IDiscordCommand[] {
    this.logger.log('Getting commands hierarchy');

    const tmpMemory = new Map<string, IDiscordCommand>();

    for (const { command, subcommand, subcommandGroup } of commands) {
      if (!tmpMemory.has(command.name)) {
        tmpMemory.set(command.name, {
          ...command,
          subcommandGroups: [],
          subcommands: [],
        });
      }

      const { subcommandGroups, subcommands } = tmpMemory.get(command.name);

      if (subcommandGroup) {
        let group = subcommandGroups.find(
          (c) => c.name === subcommandGroup.name,
        );

        if (!group) {
          group = { ...subcommandGroup, subcommands: [] };
          subcommandGroups.push(group);
        }

        group.subcommands.push(subcommand);
      } else if (subcommand) {
        subcommands.push(subcommand);
      }
    }

    return [...tmpMemory.values()];
  }
}
