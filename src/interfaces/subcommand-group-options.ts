import { ICommandBaseOptions } from '@interfaces/command-base-options';

export type ISubcommandGroupOptions = ICommandBaseOptions;
