export const AUTOROLE_MICROSERVICE = 'autorole-microservice';

export const AUTOROLE_MESSAGE_ALL_PATTERN = 'autorole:message:all';

export const AUTOROLE_MESSAGE_ENABLE_PATTERN = 'autorole:message:enable';

export const AUTOROLE_MESSAGE_DISABLE_PATTERN = 'autorole:message:disable';

export const AUTOROLE_ADD_REACTION_PATTERN = 'autorole:message:add-reaction';

export const AUTOROLE_REMOVE_REACTION_PATTERN =
  'autorole:message:remove-reaction';
