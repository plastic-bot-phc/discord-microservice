export const ADMIN_MICROSERVICE = 'admin-microservice';

export const SET_CONFESSION_CHANNEL_PATTERN =
  'admin:guild-config:set-confession-channel';

export const GET_CONFESSION_CHANNEL_PATTERN =
  'admin:guild-config:get-confession-channel';

export const GET_ADMIN_ROLES_PATTERN = 'admin:guild-config:get-admin-roles';

export const ADD_ADMIN_ROLE_PATTERN = 'admin:guild-config:add-admin-role';

export const REMOVE_ADMIN_ROLE_PATTERN = 'admin:guild-config:remove-admin-role';
