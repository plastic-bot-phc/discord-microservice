import { ICommandBaseOptions } from '@interfaces/command-base-options';
import { ICommandParameter } from '@interfaces/command-parameter';

export interface ISubcommandOptions extends ICommandBaseOptions {
  parameters?: ICommandParameter[];
}
