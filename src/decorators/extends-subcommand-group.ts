import { SUBCOMMAND_GROUP_DECORATOR } from '@constants/decorator';
import { ICommandOptions } from '@interfaces/command-options';
import { isMethod } from '@utils/is-method';

export const ExtendsSubcommandGroup = (): ClassDecorator => (target: any) => {
  const prototype = Object.getPrototypeOf(target);

  if (prototype) {
    const options: ICommandOptions = Reflect.getMetadata(
      SUBCOMMAND_GROUP_DECORATOR,
      prototype,
    );

    if (!options) {
      return target;
    }

    Reflect.defineMetadata(SUBCOMMAND_GROUP_DECORATOR, options, prototype);

    for (const propertyName of Object.getOwnPropertyNames(target.prototype)) {
      if (!isMethod(target.prototype, propertyName)) {
        continue;
      }

      Reflect.defineMetadata(
        SUBCOMMAND_GROUP_DECORATOR,
        options,
        target.prototype,
        propertyName,
      );
    }
  }

  return target;
};
