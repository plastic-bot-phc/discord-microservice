import { Command } from '@decorators/command';
import { Injectable } from '@nestjs/common';

@Injectable()
@Command({
  name: 'admin',
  description: 'Comandos de administración y configuración del servidor',
  isAdminCommand: true,
})
export class AdminBaseGateway {}
