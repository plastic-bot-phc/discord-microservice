import {
  AUTOROLE_ADD_REACTION_PATTERN,
  AUTOROLE_MESSAGE_ALL_PATTERN,
  AUTOROLE_MESSAGE_DISABLE_PATTERN,
  AUTOROLE_MESSAGE_ENABLE_PATTERN,
  AUTOROLE_MICROSERVICE,
  AUTOROLE_REMOVE_REACTION_PATTERN,
} from '@constants/microservices/autorole';
import { IMessage } from '@interfaces/autoroles/message';
import { Inject, Injectable, Logger } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { Observable } from 'rxjs';

@Injectable()
export class AutoRoleMicroservice {
  private readonly logger: Logger = new Logger(AutoRoleMicroservice.name);

  constructor(
    @Inject(AUTOROLE_MICROSERVICE)
    private readonly client: ClientProxy,
  ) {}

  getReactions(guild: string): Observable<IMessage[]> {
    return this.client.send<IMessage[]>(AUTOROLE_MESSAGE_ALL_PATTERN, {
      guild,
    });
  }

  enableMessage(guild: string, message: string): Observable<IMessage> {
    return this.client.send<IMessage>(AUTOROLE_MESSAGE_ENABLE_PATTERN, {
      guild,
      message,
    });
  }

  disableMessage(guild: string, message: string): Observable<boolean> {
    return this.client.send<boolean>(AUTOROLE_MESSAGE_DISABLE_PATTERN, {
      guild,
      message,
    });
  }

  addReaction(
    guild: string,
    message: string,
    emoji: string,
    role: string,
  ): Observable<IMessage> {
    return this.client.send<IMessage>(AUTOROLE_ADD_REACTION_PATTERN, {
      guild,
      message,
      emoji,
      role,
    });
  }

  removeReaction(
    guild: string,
    message: string,
    role: string,
  ): Observable<IMessage> {
    return this.client.send<IMessage>(AUTOROLE_REMOVE_REACTION_PATTERN, {
      guild,
      message,
      role,
    });
  }
}
