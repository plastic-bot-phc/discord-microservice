export const COMMAND_DECORATOR = 'command';

export const SUBCOMMAND_GROUP_DECORATOR = 'subcommand_group';

export const SUBCOMMAND_DECORATOR = 'subcommand';

export const REACTION_DECORATOR = 'reaction';
