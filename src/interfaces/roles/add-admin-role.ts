export interface IAddAdminRole {
  guild: string;
  role: string;
}
