import { Injectable, Logger } from '@nestjs/common';
import {
  Channel,
  Collection,
  Guild,
  GuildChannelManager,
  GuildMember,
  Message,
  Role,
  TextChannel,
  User,
} from 'discord.js';

@Injectable()
export class DiscordUtilService {
  private readonly logger: Logger = new Logger(DiscordUtilService.name);

  async getMessageById(
    guildChannelManager: GuildChannelManager,
    messageId: string,
  ): Promise<Message> {
    const data: Collection<string, Channel> = await guildChannelManager.fetch();

    const channels: TextChannel[] = data
      .filter((channel) => channel.isText())
      .toJSON() as TextChannel[];

    for (let channel of channels) {
      try {
        const message = await channel.messages.fetch(messageId);
        return message;
      } catch (e) {
        this.logger.warn(
          `Message with ID ${messageId} not found on channel ${channel.name}`,
        );
      }
    }

    this.logger.warn(`Couldn't find message on GUILD`);

    return null;
  }

  async addRoleToUser(
    guild: Guild,
    user: User,
    roleId: string,
  ): Promise<boolean> {
    const role: Role = await guild.roles.fetch(roleId);
    const member: GuildMember = await guild.members.fetch(user.id);

    if (member && role) {
      this.logger.log(`Adding role ${role.name} to member ${user.username}`);
      await member.roles.add(role);
    }

    return true;
  }

  async removeRoleToUser(
    guild: Guild,
    user: User,
    roleId: string,
  ): Promise<boolean> {
    const role: Role = await guild.roles.fetch(roleId);
    const member: GuildMember = await guild.members.fetch(user.id);

    if (member && role) {
      this.logger.log(`Removing role ${role.name} to member ${user.username}`);
      await member.roles.remove(role);
    }

    return true;
  }
}
