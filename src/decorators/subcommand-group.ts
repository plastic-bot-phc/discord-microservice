import { SUBCOMMAND_GROUP_DECORATOR } from '@constants/decorator';
import { ISubcommandGroupOptions } from '@interfaces/subcommand-group-options';
import { isMethod } from '@utils/is-method';

export const SubcommandGroup =
  (options: ISubcommandGroupOptions): MethodDecorator & ClassDecorator =>
  (
    target: any,
    propertyKey?: string | symbol,
    descriptor?: TypedPropertyDescriptor<any>,
  ) => {
    if (descriptor) {
      Reflect.defineMetadata(
        SUBCOMMAND_GROUP_DECORATOR,
        options,
        target,
        propertyKey,
      );
    } else {
      Reflect.defineMetadata(SUBCOMMAND_GROUP_DECORATOR, options, target);

      for (const propertyName of Object.getOwnPropertyNames(target.prototype)) {
        if (!isMethod(target, propertyName)) {
          continue;
        }

        Reflect.defineMetadata(
          SUBCOMMAND_GROUP_DECORATOR,
          options,
          target,
          propertyName,
        );
      }
    }

    return target;
  };
