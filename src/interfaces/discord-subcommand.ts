import { ISubcommandOptions } from '@interfaces/subcommand-options';

export type IDiscordSubcommand = ISubcommandOptions;
