import { ExtendsCommand } from '@decorators/extends-command';
import { Subcommand } from '@decorators/subcommand';
import { CommandParameterType } from '@enums/command-parameter-type';
import { AdminBaseGateway } from '@gateways/admin/admin-base';
import { AdminMicroservice } from '@microservices/admin';
import { Injectable, Logger } from '@nestjs/common';
import { CommandInteraction, GuildChannel } from 'discord.js';
import { from, Observable, switchMap } from 'rxjs';

@Injectable()
@ExtendsCommand()
export class AdminConfessionGateway extends AdminBaseGateway {
  private readonly logger: Logger = new Logger(AdminConfessionGateway.name);

  constructor(private readonly adminMicroservice: AdminMicroservice) {
    super();
  }

  @Subcommand({
    name: 'confesiones',
    description: 'Configura el canal para publicar confesiones',
    parameters: [
      {
        name: 'canal',
        description:
          'Canal donde se publicaran las confesiones al utilizar el comando',
        type: CommandParameterType.Channel,
        required: true,
      },
    ],
  })
  setConfessionsChannel(interaction: CommandInteraction): Observable<any> {
    const channel: GuildChannel = <GuildChannel>(
      interaction.options.getChannel('canal')
    );

    return from(interaction.deferReply({ ephemeral: true })).pipe(
      switchMap(() =>
        this.adminMicroservice.setConfessionChannel(
          interaction.guildId,
          channel.id,
        ),
      ),
      switchMap(() =>
        interaction.editReply('Canal para confesiones actualizado'),
      ),
    );
  }
}
