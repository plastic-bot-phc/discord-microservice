import { ExtendsCommand } from '@decorators/extends-command';
import { ExtendsSubcommandGroup } from '@decorators/extends-subcommand-group';
import { Subcommand } from '@decorators/subcommand';
import { CommandParameterType } from '@enums/command-parameter-type';
import { AdminRoleBaseGateway } from '@gateways/admin/admin-role-base';
import { AdminMicroservice } from '@microservices/admin';
import { Injectable, Logger } from '@nestjs/common';
import { DiscordRxService } from '@services/discord-rx';
import { CommandInteraction, Role } from 'discord.js';
import { from, map, Observable, switchMap } from 'rxjs';

@Injectable()
@ExtendsCommand()
@ExtendsSubcommandGroup()
export class AdminRoleGateway extends AdminRoleBaseGateway {
  private readonly logger: Logger = new Logger(AdminRoleGateway.name);

  constructor(
    private readonly adminMicroservice: AdminMicroservice,
    private readonly discordRxService: DiscordRxService,
  ) {
    super();
  }

  @Subcommand({
    name: 'agregar',
    description:
      'Agrega un nuevo rol para usar los comandos exclusivos de administrador',
    parameters: [
      {
        name: 'rol',
        description:
          'Rol que podrá usar los comandos exclusivos para administradores',
        type: CommandParameterType.Role,
        required: true,
      },
    ],
  })
  addAdminRole(interaction: CommandInteraction): Observable<any> {
    const role: Role = <Role>interaction.options.getRole('rol');

    return from(interaction.deferReply({ ephemeral: true })).pipe(
      switchMap(() =>
        this.adminMicroservice.addAdminRole(interaction.guildId, role.id),
      ),
      map(() =>
        this.discordRxService.emitAddRolePermission(
          interaction.guildId,
          role.id,
        ),
      ),
      switchMap(() =>
        interaction.editReply('Rol agregado como administrador del bot'),
      ),
    );
  }

  @Subcommand({
    name: 'lista',
    description:
      'Lista de los roles que pueden utilizar los comandos exclusivos de administradores',
  })
  getAdminRoles(interaction: CommandInteraction): Observable<any> {
    return from(interaction.deferReply({ ephemeral: true })).pipe(
      switchMap(() =>
        this.adminMicroservice.getAdminRoles(interaction.guildId),
      ),
      switchMap((roles: string[]) =>
        interaction.editReply(
          roles.length
            ? roles.map((id) => `<@&${id}>`).join('\n')
            : 'No hay roles configurados',
        ),
      ),
    );
  }

  @Subcommand({
    name: 'eliminar',
    description:
      'Elimina un rol para evitar usar los comandos exclusivos de administrador',
    parameters: [
      {
        name: 'rol',
        description:
          'Rol que podrá usar los comandos exclusivos para administradores',
        type: CommandParameterType.Role,
        required: true,
      },
    ],
  })
  removeAdminRole(interaction: CommandInteraction): Observable<any> {
    const role: Role = <Role>interaction.options.getRole('rol');

    return from(interaction.deferReply({ ephemeral: true })).pipe(
      switchMap(() =>
        this.adminMicroservice.removeAdminRole(interaction.guildId, role.id),
      ),
      map(() =>
        this.discordRxService.emitRemoveRolePermission(
          interaction.guildId,
          role.id,
        ),
      ),
      switchMap(() =>
        interaction.editReply('Rol eliminado como administrador del bot'),
      ),
    );
  }
}
