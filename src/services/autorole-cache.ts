import { IMessage } from '@interfaces/autoroles/message';
import { AutoRoleMicroservice } from '@microservices/autorole';
import { CACHE_MANAGER, Inject, Injectable, Logger } from '@nestjs/common';
import { Cache } from 'cache-manager';
import { from, Observable, switchMap, map, mergeMap } from 'rxjs';

type CacheRole = {
  guild: string;
  message: string;
  emoji: string;
  role: string;
};

@Injectable()
export class AutoRoleCacheService {
  private readonly logger: Logger = new Logger(AutoRoleCacheService.name);

  constructor(
    @Inject(CACHE_MANAGER) private readonly cacheManager: Cache,
    private readonly autoroleMicroservice: AutoRoleMicroservice,
  ) {}

  sync(guild: string): Observable<any> {
    return this.autoroleMicroservice.getReactions(guild).pipe(
      map((messages: IMessage[]) =>
        messages.flatMap((message) =>
          message.roles.map((r) => ({
            guild: message.guild,
            message: message.message,
            emoji: r.emoji,
            role: r.role,
          })),
        ),
      ),
      switchMap((roles: CacheRole[]) => from(roles)),
      mergeMap((role: CacheRole) =>
        this.set(role.guild, role.message, role.emoji, role.role),
      ),
    );
  }

  async get(
    guild: string,
    message: string,
    emoji: string,
  ): Promise<string | null> {
    return this.cacheManager.get(this.getKey(guild, message, emoji));
  }

  async set(
    guild: string,
    message: string,
    emoji: string,
    role: string,
  ): Promise<boolean> {
    await this.cacheManager.set(this.getKey(guild, message, emoji), role, {
      ttl: 0,
    });

    return true;
  }

  async del(guild: string, message: string, emoji: string): Promise<boolean> {
    await this.cacheManager.del(this.getKey(guild, message, emoji));
    return true;
  }

  private getKey(guild: string, message: string, emoji: string): string {
    return `${guild}:${message}:${emoji}`;
  }
}
