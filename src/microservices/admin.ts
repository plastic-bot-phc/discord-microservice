import {
  ADD_ADMIN_ROLE_PATTERN,
  ADMIN_MICROSERVICE,
  GET_ADMIN_ROLES_PATTERN,
  GET_CONFESSION_CHANNEL_PATTERN,
  REMOVE_ADMIN_ROLE_PATTERN,
  SET_CONFESSION_CHANNEL_PATTERN,
} from '@constants/microservices/admin';
import { Inject, Injectable, Logger } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { Observable } from 'rxjs';

@Injectable()
export class AdminMicroservice {
  private readonly logger: Logger = new Logger(AdminMicroservice.name);

  constructor(
    @Inject(ADMIN_MICROSERVICE)
    private readonly client: ClientProxy,
  ) {}

  setConfessionChannel(guild: string, channel: string): Observable<void> {
    return this.client.send<void>(SET_CONFESSION_CHANNEL_PATTERN, {
      guild,
      channel,
    });
  }

  getConfessionChannel(guild: string): Observable<string> {
    return this.client.send<string>(GET_CONFESSION_CHANNEL_PATTERN, {
      guild,
    });
  }

  addAdminRole(guild: string, role: string): Observable<void> {
    this.logger.log('addAdminRole', { guild, role });

    return this.client.send<void>(ADD_ADMIN_ROLE_PATTERN, {
      guild,
      role,
    });
  }

  getAdminRoles(guild: string): Observable<string[]> {
    return this.client.send<string[]>(GET_ADMIN_ROLES_PATTERN, { guild });
  }

  removeAdminRole(guild: string, role: string): Observable<void> {
    return this.client.send<void>(REMOVE_ADMIN_ROLE_PATTERN, {
      guild,
      role,
    });
  }
}
