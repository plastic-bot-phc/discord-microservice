import { ExtendsCommand } from '@decorators/extends-command';
import { SubcommandGroup } from '@decorators/subcommand-group';
import { AdminBaseGateway } from '@gateways/admin/admin-base';
import { Injectable } from '@nestjs/common';

@Injectable()
@ExtendsCommand()
@SubcommandGroup({
  name: 'roles',
  description:
    'Roles que pueden hacer uso de los comandos exclusivos para administradores',
})
export class AdminRoleBaseGateway extends AdminBaseGateway {}
