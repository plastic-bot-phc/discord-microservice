import { REDIS_URI_ENV, REDIS_PASSWORD_ENV } from '@constants/environment';
import { ADMIN_MICROSERVICE } from '@constants/microservices/admin';
import { AUTOROLE_MICROSERVICE } from '@constants/microservices/autorole';
import { AdminAutoRoleGateway } from '@gateways/admin/admin-autorole';
import { AdminConfessionGateway } from '@gateways/admin/admin-confession';
import { AdminRoleGateway } from '@gateways/admin/admin-role';
import { ConfessionGateway } from '@gateways/nsfw/confession';
import { AdminMicroservice } from '@microservices/admin';
import { AutoRoleMicroservice } from '@microservices/autorole';
import { CacheModule, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { DiscoveryModule } from '@nestjs/core';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { AutoRoleCacheService } from '@services/autorole-cache';
import { BotService } from '@services/bot';
import { CommandDiscoveryService } from '@services/command-discovery';
import { DiscordClientService } from '@services/discord-client';
import { DiscordRxService } from '@services/discord-rx';
import { DiscordUtilService } from '@services/discord-util';
import { ReactionDiscoveryService } from '@services/reaction-discovery';
import * as redisStore from 'cache-manager-redis-store';

@Module({
  imports: [
    ConfigModule.forRoot(),
    DiscoveryModule,
    ClientsModule.registerAsync([
      {
        inject: [ConfigService],
        imports: [ConfigModule],
        name: ADMIN_MICROSERVICE,
        useFactory: (configService: ConfigService) => ({
          transport: Transport.REDIS,
          options: {
            host: configService.get(REDIS_URI_ENV),
            password: configService.get(REDIS_PASSWORD_ENV),
          },
        }),
      },
      {
        inject: [ConfigService],
        imports: [ConfigModule],
        name: AUTOROLE_MICROSERVICE,
        useFactory: (configService: ConfigService) => ({
          transport: Transport.REDIS,
          options: {
            host: configService.get(REDIS_URI_ENV),
            password: configService.get(REDIS_PASSWORD_ENV),
          },
        }),
      },
    ]),
    CacheModule.registerAsync({
      inject: [ConfigService],
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        store: redisStore,
        host: configService.get(REDIS_URI_ENV),
        password: configService.get(REDIS_PASSWORD_ENV),
      }),
    }),
  ],
  providers: [
    BotService,
    DiscordClientService,
    CommandDiscoveryService,
    ReactionDiscoveryService,
    //AdminConfessionGateway,
    //AdminRoleGateway,
    ConfessionGateway,
    AdminMicroservice,
    DiscordRxService,
    //AdminAutoRoleGateway,
    AutoRoleMicroservice,
    DiscordUtilService,
    AutoRoleCacheService,
  ],
})
export class AppModule {}
