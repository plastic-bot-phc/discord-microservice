import { CommandParameterType } from '@enums/command-parameter-type';
import { ICommandBaseOptions } from '@interfaces/command-base-options';

export interface ICommandParameter extends ICommandBaseOptions {
  type: CommandParameterType;
  required?: boolean;
}
