import { ICommandOptions } from '@interfaces/command-options';
import { IDiscordSubcommand } from '@interfaces/discord-subcommand';
import { IDiscordSubcommandGroup } from '@interfaces/discord-subcommand-group';

export interface IDiscordCommand extends ICommandOptions {
  subcommandGroups?: IDiscordSubcommandGroup[];
  subcommands?: IDiscordSubcommand[];
}
